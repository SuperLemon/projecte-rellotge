﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace reloj
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        MediaPlayer mp3Player = new MediaPlayer();
        DispatcherTimer disp1 = new DispatcherTimer();
        DispatcherTimer alarmastop = new DispatcherTimer();
        private MediaPlayer mediaPlayer = new MediaPlayer();
        OpenFileDialog openFileDialog = new OpenFileDialog();
        const string FileName = ".\\AlarmaG.bin";
        private Alarma alarm = new Alarma(0,0,0, false);
        Window1 w1 = new Window1();

        private Alarma alarma;
        public MainWindow()
        {
            alarma = new Alarma();
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            lblTime.Content = DateTime.Now.ToLongTimeString();
        }

        private void filemenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Ivan Parra");
        }

        private void activa_Click_1(object sender, RoutedEventArgs e)
        {
            desactivado.IsChecked = false;

        }

        private void desactiva_Click_desact(object sender, RoutedEventArgs e)
        {
            activado.IsChecked = false;
            disp1.Stop();
        }

        private void activa_Checked(object sender, RoutedEventArgs e)
        {
            
            w1.ShowDialog();
            int hora = int.Parse(w1.horas.Text);
            int min = int.Parse(w1.minutos.Text);
            int sec = int.Parse(w1.segundos.Text);
            disp1.Tick += new EventHandler(DispatcherTimer_Tick);
                disp1.Interval = new TimeSpan(hora, min, sec);
                disp1.Start();
            
        }

        
        
        
    private void DispatcherTimer_Tick(object sender, EventArgs e) {
            Uri mp3 = new Uri("C:\\Users\\Ambrosio69\\Downloads\\perforar_1.mp3");
            mp3Player.Open(mp3);
            mp3Player.Play();

            MessageBox.Show("Alarma");

            activado.IsChecked = false;
            desactivado.IsChecked = true;
            disp1.Stop();
            
            alarmastop.Tick += new EventHandler(alarmaTimer_Tick);
            alarmastop.Interval = new TimeSpan(0, 0, 4);
            alarmastop.Start();

        }

        private void alarmaTimer_Tick(object sender, EventArgs e)
        {
            mp3Player.Stop();
            alarmastop.Stop();
        }

        private EventHandler EventHandler(object alarmaTimer_Tick)
        {
            throw new NotImplementedException();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists(FileName))
            {
                Stream TestFileStream = File.OpenRead(FileName);
                BinaryFormatter deserializer = new BinaryFormatter();
                alarm = (Alarma) deserializer.Deserialize(TestFileStream);
                TestFileStream.Close();
                activado.IsChecked = alarm.alarmaActiva;
                w1.horas.Text = alarm.H.ToString();
                w1.minutos.Text = alarm.Min.ToString();
                w1.segundos.Text = alarm.Secs.ToString();

            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            alarm.H = int.Parse(w1.horas.Text);
            alarm.Min = int.Parse(w1.minutos.Text);
            alarm.Secs = int.Parse(w1.segundos.Text);

            Stream TestFileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(TestFileStream, alarm);
        }

        
    }
}
